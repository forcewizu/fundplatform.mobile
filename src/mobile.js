window.onload = function() {

  let $buttons = document.querySelectorAll('.m-button-drop');
	
  for (let i = 0; i < $buttons.length; i++) {
    $buttons[i].addEventListener('click', function() {
		let $drop = this.closest('.m-section').querySelector('.m-drop'),
			$prev = this.previousElementSibling,
			$buttonWrapper = this.closest('div'),
			$image = this.querySelector('.m-button-drop__image'),
			$buttonNames = this.querySelectorAll('.m-button-drop__name');
			for(let i = 0; i < $buttonNames.length; i++) {
				$buttonNames[i].classList.toggle('m-show');
			}
      	if (!$drop.style.maxHeight) {
			$drop.style.paddingBottom = '24px';
			$drop.style.maxHeight = $drop.scrollHeight + "px";
			$buttonWrapper.classList.add('m-buttons-col');
			$image.classList.add('m-rotate');
			if($prev) $prev.classList.add('m-prev')
      	} else {
			$drop.style.paddingBottom = '0';
			$drop.style.maxHeight = null;
			$buttonWrapper.classList.remove('m-buttons-col');
			$image.classList.remove('m-rotate');
			if($prev) $prev.classList.remove('m-prev')
		}
    });
	}
	
	let $burger = document.getElementById('m-burger'),
			$burgerMenu = document.getElementById('m-drop-menu');
	$burger.addEventListener('click', function() {
		if (!$burgerMenu.style.maxHeight) {
			$burgerMenu.style.maxHeight = "50vh";
			$burger.classList.add('m-active');
		} else {
			$burgerMenu.style.maxHeight = null;
			$burger.classList.remove('m-active');
		}
	})
};
